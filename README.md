
# Rewardify Project

Rewardify is a web application designed to manage and distribute rewards. This project aims to simplify the process of reward allocation and tracking for organizations.

## Table of Contents
- [Video Walkthrough](#Walkthrough)
- Credentials(#Credentials)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [Pages Overview](#pages-overview)
- [Contributing](#contributing)
- [License](#license)

## Walkthrough

- Video Link: https://drive.google.com/file/d/1tsnep8cwHTse9jBvR9J8rxtMIXRzetUT/view?usp=sharing

##Credentials
- Admin username-  `Admin_acct`
- Admin pass- `Admin@8745`
- User name- `Test2`
- User pass- `Test@8745`

## Features

- User authentication and management
- Admin dashboard for reward management
- Responsive design
- Integration with external services

## Installation

1. Clone the repository:

   ```bash
   git clone https://github.com/PriyabrataP00/rewardify_project.git
   cd rewardify_project
   ```

2. Create a virtual environment and activate it:

   ```bash
   python -m venv venv
   source venv/bin/activate  # On Windows use `venv\Scripts\activate`
   ```

3. Install the required dependencies:

   ```bash
   pip install -r requirements.txt
   ```

4. Apply migrations:

   ```bash
   python manage.py makemigrations
   python manage.py migrate
   ```

5. Create a superuser:

   ```bash
   python manage.py createsuperuser
   ```

6. **Add necessary environment variables.** Ensure you have set up all required environment variables, such as database settings, secret keys, etc.

7. Run the development server:

   ```bash
   python manage.py runserver
   ```

## Usage

1. Open your web browser and navigate to `http://127.0.0.1:8000/`.
2. Log in with the superuser credentials created earlier.
3. Use the "Create Admin" feature within the application to add new admins as needed.
4. Manage rewards and users through the provided interfaces.

## Project Structure

- `admin_component/` - Contains the admin-related functionalities.
- `user_component/` - Handles user-related operations.
- `rewardify_project/` - Main project settings and configurations.
- `app_images/` - Stores images used in the application.
- `media/` - Media files for the application.
- `requirements.txt` - List of dependencies.
- `manage.py` - Django's command-line utility.

## Pages Overview

### Admin Pages

1. **Home**: Overview of the added applications.
2. **Add Apps**: Functionality to add new applications.
3. **Settings**: Configure application settings such as creating, editing or deleting Categories and SubCategories .
4. **Create Admin**: Feature to create new admin users.

### User Pages

1. **Home**: User-specific dashboard showing completed tasks.
2. **Profile**: View  personal information and preferences.
3. **Points**: Track and view accumulated points alongwith a leaderboard.
4. **Tasks**: View and manage tasks assigned to the user.


### Problem Set 1:
 The given structure is a dictionary. The values of the dictionary are lists. For `orders` key we have a list of dictionaries as values. From the list of dictionary we have to extract the value of the `id` keys. The code to do so is given below:

```python
data = {"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],
        "errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

order_ids = [order['id'] for order in data['orders']]
print(order_ids)
```

### Problem Set 3:
### PART A
For scheduling periodic tasks such as downloading a list of ISINs every 24 hours, the first thing that comes to my mind is to use cron jobs on a Unix-like system. 

The reasons behind setting up a cron job would be:
1.  cron is straightforward to set up and use. Tasks can be easily scheduled with simple and easy syntaxes. It's easy to understand for anyone familiar with Unix/Linux systems.
2. Cron is pre-installed on almost all Unix like operating systems thus removing the requirement of installing additional software.
3. cron has been around for decades and is known for its reliability. Once set up, it will run the tasks at the specified intervals without requiring much maintenance.
4.  cron is lightweight and doesn't consume significant system resources.

The potential Issues with cron are:
1.  cron is not designed for high scalability or for managing a large number of scheduled tasks across multiple servers. It operates on a single server, which can be a limitation in distributed environments.
2.  While cron can send output to log files or email addresses, it lacks advanced error handling and notification mechanisms that might be needed in more complex systems.
3.  There's no built-in monitoring system to track the status of jobs. Monitoring will require additional setups.

**Recommendations for Production**
For more complex or large-scale systems, we need to use more advanced scheduling and orchestration tools such as Celery.
1.  Celery is an asynchronous task queue/job queue that is focused on real-time operation. By using Celery Beat in conjunction with Celery we can achieve scheduling. It provides robust error handling, retries, and monitoring.
2.  Celery can be distributed across multiple worker nodes, which makes it highly scalable.

Apart from these we can also use Apache Airflow or Kubernetes CronJobs for scheduling in production environments.

### PART B
1. Flask is usually used for smaller applications, microservices, and projects that require high customization and a lot more flexibility. Flask provides maximum control over the architecture. We can quickly prototype and iterate on ideas.
2. Django is often used for full-featured, large-scale applications that benefit from a structured framework with many built-in features. It is ideal for projects that need built-in security, rapid development, and scalability.
3. Both Flask and Django are powerful frameworks, each having its own strengths and ideal use cases. The major factor behind choosing between them depends on the specific requirements of the project, the complexity of the application, and the long-term goals of the project.

## Contributing

Contributions are welcome! Please follow these steps:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -m 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
